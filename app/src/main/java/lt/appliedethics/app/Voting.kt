package lt.appliedethics.app

import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.ln

class Voting {

    fun calculate(votes: List<Vote>): Result {
        val medianContribution = calculateNonZeroMedianContribution(votes)

        var votedYesTotal = BigDecimal.ZERO
        var votedNoTotal = BigDecimal.ZERO

        val weightedVotes = votes.map { vote ->
            val voteCoefficientDbl = 1 + ln(1 + vote.contribution.toDouble()/medianContribution.toDouble())
            val voteCoefficient = BigDecimal(voteCoefficientDbl).setScale(4, RoundingMode.HALF_UP)
            if (vote.isYes == true) {
                votedYesTotal = votedYesTotal.plus(voteCoefficient)
            } else if (vote.isYes == false){
                votedNoTotal = votedNoTotal.plus(voteCoefficient)
            }
            vote.apply { this.voteCoefficient = voteCoefficient }
        }

        return Result(
            medianContribution = medianContribution,
            weightedVotes,
            votedYesTotal,
            votedNoTotal
        )
    }

    private fun calculateNonZeroMedianContribution(votes: List<Vote>): BigDecimal {
        val midIndex = votes.size / 2
        val sortedVotes = votes.sortedBy { it.contribution }

        var medianContribution = if (votes.size % 2 == 0) {
            sortedVotes[midIndex].contribution
                .plus(sortedVotes[midIndex - 1].contribution)
                .divide(BigDecimal(2), 1, RoundingMode.HALF_UP)
        } else {
            sortedVotes[midIndex].contribution
        }

        /**
        * This gives disproportional power to the contributing members in the situation where more
        * than half of the members no longer contribute
        **/
        if (medianContribution.compareTo(BigDecimal.ZERO) == 0) {
            medianContribution = BigDecimal(0.1).setScale(1)
        }

        return medianContribution
    }

    data class Result(
        val medianContribution: BigDecimal,
        val weightedVotes: List<Vote>,
        val yesVoteTotal: BigDecimal,
        val noVoteTotal: BigDecimal,
    )
}

data class Vote(
    val id: Int = 0,
    var contribution: BigDecimal = BigDecimal.ZERO,
    var voteCoefficient: BigDecimal = BigDecimal.ONE,
    var voter: String = "",
    var isYes: Boolean? = null,
)