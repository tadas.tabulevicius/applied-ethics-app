/*
 * Applied Ethics app.
 * Copyright© 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.appliedethics.app.demo

import androidx.annotation.StringRes
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import lt.appliedethics.app.R
import lt.appliedethics.app.Vote
import lt.appliedethics.app.Voting
import java.math.BigDecimal

class DemoViewModel: ViewModel() {
    val votes = mutableStateListOf(Vote())
    val demoScreenData = mutableStateOf(DemoScreenData())
    private var currentId = 1

    fun onNameChange(id: Int, name: String) {
        if (name.isNotEmpty()) {
            votes.firstOrNull { it.id == id }?.apply { voter = name }
        }
    }
    fun onContributionChange(id: Int, contribution: String) {
        demoScreenData.value = DemoScreenData(result = null)
        if (contribution.isNotEmpty()) {
            try {
                val contributionBigDecimal = BigDecimal(contribution)
                votes.firstOrNull { it.id == id }
                    ?.apply { this.contribution = contributionBigDecimal }
            } catch (e: Exception) {
                demoScreenData.value = DemoScreenData(
                    toastMessageId = R.string.toast_invalid_contribution
                )
            }
        }
    }

    fun onVoteChange(id: Int, isYesVote: Boolean) {
        demoScreenData.value = DemoScreenData(result = null)
        votes.firstOrNull { it.id == id }?.apply { this.isYes = isYesVote }
    }

    fun onVoteAdd() {
        votes.add(Vote(id = currentId++))
    }

    fun onVoteDelete(id: Int) {
        votes.firstOrNull { it.id == id }?.let { votes.remove(it) }
    }

    fun onToastMessageDisplayed() {
        demoScreenData.value = DemoScreenData()
    }

    fun onCalculateResults() {
        try {
            val results = Voting().calculate(votes)
            demoScreenData.value = DemoScreenData(result = createResultString(results))
        } catch (e: Exception) {
            demoScreenData.value = DemoScreenData(toastMessageId = R.string.toast_incomplete_data)
        }
    }

    private fun createResultString(results: Voting.Result): String {
        var resultText = "Total Yes votes: ${results.yesVoteTotal}\n" +
                "Total No votes: ${results.noVoteTotal}\n" +
                "Median Contribution: ${results.medianContribution}\n" +
                "Votes:\n"

        for (vote in results.weightedVotes) {
            val voteText = "${vote.voter} ${vote.contribution} " +
                    if (vote.isYes == true) {
                        "Yes ${vote.voteCoefficient}\n"
                    } else {
                        "No ${vote.voteCoefficient}\n"
                    }
            resultText += voteText
        }

        return resultText
    }
}

class DemoViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DemoViewModel::class.java)) {
            return DemoViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

data class DemoScreenData(
    @StringRes val toastMessageId: Int = 0,
    val result: String? = null,
)




