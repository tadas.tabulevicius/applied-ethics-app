/*
 * Applied Ethics app.
 * Copyright© 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(ExperimentalMaterial3Api::class)

package lt.appliedethics.app.demo

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import lt.appliedethics.app.R
import lt.appliedethics.app.Vote

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DemoScreen(
    votes: List<Vote>,
    result: String? = null,
    navigateToInfoScreen: () -> Unit,
    onNameChange: (Int, String) -> Unit,
    onContributionChange: (Int, String) -> Unit,
    onVoteChange: (Int, Boolean) -> Unit,
    onVoteAdd: () -> Unit,
    onVoteDelete: (Int) -> Unit,
    onCalculateResults: () -> Unit,
) {
    Surface(
        modifier = Modifier
            .fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.padding(top = 16.dp)
        ) {
            item {
                ScreenTitle()
            }

            for (vote in votes) {
                item {
                    VoteCard(
                        vote = vote,
                        onNameChange = onNameChange,
                        onContributionChange = onContributionChange,
                        onVoteChange = onVoteChange,
                        onVoteDelete = onVoteDelete,
                    )
                }
            }

            item {
                AddVoteIcon(onVoteAdd)
            }

            item {
                CalculateResultsIcon(onCalculateResults)
            }

            result?.let {
                item {
                    VotingResults(it)
                }
            }

            item {
                InfoButton(navigateToInfoScreen)
            }
        }
    }
}

@Composable
private fun ScreenTitle() {
    Text(
        text = stringResource(R.string.demo_screen_title),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(16.dp)
    )
}

@Composable
private fun AddVoteIcon(onVoteAdd: () -> Unit) {
    Image(
        painter = painterResource(id = R.drawable.ic_add_circle),
        contentDescription = null,
        Modifier.clickable(onClick = { onVoteAdd() })
    )
}

@Composable
private fun CalculateResultsIcon(onCalculateResults: () -> Unit) {
    Image(
        painter = painterResource(id = R.drawable.ic_results),
        contentDescription = null,
        Modifier.clickable(onClick = { onCalculateResults() })
    )
}

@Composable
private fun VotingResults(it: String) {
    Text(
        text = it,
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(16.dp)
    )
}

@Composable
private fun InfoButton(navigateToInfoScreen: () -> Unit) {
    Image(
        painter = painterResource(id = R.drawable.ic_launcher_round),
        contentDescription = null,
        modifier = Modifier
            .padding(16.dp)
            .size(48.dp)
            .clip(CircleShape)
            .clickable { navigateToInfoScreen() }
    )
}

@Composable
fun VoteCard(
    vote: Vote,
    onNameChange: (Int, String) -> Unit,
    onContributionChange: (Int, String) -> Unit,
    onVoteChange: (Int, Boolean) -> Unit,
    onVoteDelete: (Int) -> Unit,
    modifier: Modifier = Modifier,
) {
    Surface(
        shape = MaterialTheme.shapes.small,
        color = MaterialTheme.colorScheme.surface,
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colorScheme.outline
        ),
        modifier = modifier
            .padding(horizontal = 8.dp)
            .clip(MaterialTheme.shapes.small)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            NameTextField(vote, onNameChange)

            ContributionTextField(vote, onContributionChange)

            VoteOptionsTitle()

            Row {
                val radioOptions = listOf(
                    stringResource(R.string.vote_option_yes),
                    stringResource(R.string.vote_option_no),
                )
                val initialSelection = when (vote.isYes) {
                    true -> radioOptions[0]
                    false -> radioOptions[1]
                    else -> ""
                }
                var selectedOption by remember { mutableStateOf(initialSelection) }

                Column(
                    Modifier
                        .selectableGroup()
                        .weight(5f)
                ) {
                    radioOptions.forEach { text ->
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .height(56.dp)
                                .selectable(
                                    selected = (text == selectedOption),
                                    onClick = {
                                        selectedOption = text
                                        onVoteChange(vote.id, text == radioOptions[0])
                                    },
                                    role = Role.RadioButton
                                )
                                .padding(horizontal = 16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = (text == selectedOption),
                                onClick = null
                            )
                            Text(
                                text = text,
                                style = MaterialTheme.typography.bodyLarge,
                                modifier = Modifier.padding(start = 16.dp)
                            )
                        }
                    }
                }

                Image(
                    painter = painterResource(id = R.drawable.ic_delete),
                    contentDescription = null,
                    Modifier
                        .clickable(onClick = { onVoteDelete(vote.id) })
                        .weight(1f)
                )
            }
        }
    }
}

@Composable
private fun VoteOptionsTitle() {
    Text(
        text = stringResource(R.string.vote_options_title),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                start = 16.dp,
                top = 16.dp,
                end = 16.dp,
                bottom = 0.dp
            ),
    )
}

@Composable
private fun ContributionTextField(
    vote: Vote,
    onContributionChange: (Int, String) -> Unit
) {
    var contribution by remember { mutableStateOf(vote.contribution.toPlainString()) }
    TextField(
        value = contribution,
        onValueChange = {
            contribution = it
            onContributionChange(vote.id, it)
        },
        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
        label = { Text(stringResource(R.string.contribution)) },
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
    )
}

@Composable
private fun NameTextField(
    vote: Vote,
    onNameChange: (Int, String) -> Unit
) {
    var name by remember { mutableStateOf(vote.voter) }
    TextField(
        value = name,
        onValueChange = {
            name = it
            onNameChange(vote.id, it)
        },
        label = { Text(stringResource(R.string.name)) },
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
    )
}


