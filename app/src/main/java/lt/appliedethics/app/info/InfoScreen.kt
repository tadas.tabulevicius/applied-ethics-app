/*
 * Applied ethics app.
 * Copyright© 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package lt.appliedethics.app.info

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.unit.dp
import lt.appliedethics.app.R
import lt.appliedethics.app.BuildConfig

@Composable
fun InfoScreen() {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        Column {
            Version()

            Text(
                text = stringResource(R.string.license),
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(16.dp)
            )

            UrlTitle(R.string.code_title)
            Url(R.string.code_url)

            UrlTitle(R.string.how_to_why_title)
            Url(R.string.how_to_why_url)
        }
    }
}

@Composable
private fun UrlTitle(@StringRes sourceCodeTitleId: Int) {
    Text(
        text = stringResource(sourceCodeTitleId),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(
            start = 16.dp,
            top = 16.dp,
            end = 16.dp,
            bottom = 8.dp
        )
    )
}

@Composable
private fun Url(@StringRes urlId: Int) {
    val urlString = stringResource(urlId)
    val uriHandler = LocalUriHandler.current
    SelectionContainer {
        val smallTitleStyle = MaterialTheme.typography.titleSmall
        ClickableText(
            text = AnnotatedString(
                urlString,
                SpanStyle(
                    MaterialTheme.colorScheme.primary,
                    smallTitleStyle.fontSize,
                    smallTitleStyle.fontWeight,
                    smallTitleStyle.fontStyle
                )
            ),
            onClick = { uriHandler.openUri(urlString) },
            modifier = Modifier.padding(
                start = 16.dp,
                top = 0.dp,
                end = 16.dp,
                bottom = 16.dp
            )
        )
    }
}

@Composable
private fun Version() {
    Text(
        text = stringResource(R.string.version, BuildConfig.VERSION_NAME),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(16.dp)
    )
}
