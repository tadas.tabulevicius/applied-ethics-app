/*
 * Applied Ethics app.
 * Copyright© 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.appliedethics.app

import android.app.Activity
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import lt.appliedethics.app.Destinations.DEMO_SCREEN
import lt.appliedethics.app.Destinations.INFO_SCREEN
import lt.appliedethics.app.demo.DemoScreen
import lt.appliedethics.app.demo.DemoViewModel
import lt.appliedethics.app.demo.DemoViewModelFactory
import lt.appliedethics.app.info.InfoScreen

object Destinations {
    const val DEMO_SCREEN = "SPRINGS_SCREEN"
    const val INFO_SCREEN = "INFO_SCREEN"
}

@Composable
fun AppliedEthicsNavHost(navController: NavHostController = rememberNavController()) {
    NavHost(
        navController = navController,
        startDestination = DEMO_SCREEN,
    ) {
        composable(DEMO_SCREEN) {
            DoubleBackHandler()
            val viewModel: DemoViewModel = viewModel(
                factory = DemoViewModelFactory()
            )
            DemoScreen(
                viewModel.votes,
                viewModel.demoScreenData.value.result,
                navigateToInfoScreen = { navController.navigate(INFO_SCREEN) },
                onNameChange = { id, name -> viewModel.onNameChange(id, name) },
                onContributionChange = { id, name -> viewModel.onContributionChange(id, name) },
                onVoteChange = { id, isYesVote -> viewModel.onVoteChange(id, isYesVote) },
                onVoteAdd = { viewModel.onVoteAdd() },
                onVoteDelete = { id -> viewModel.onVoteDelete(id) },
                onCalculateResults = { viewModel.onCalculateResults() },
            )

            val toastMessageId = viewModel.demoScreenData.value.toastMessageId
            if (toastMessageId != 0) {
                Toast.makeText(LocalContext.current, toastMessageId, Toast.LENGTH_SHORT)
                    .show()
                viewModel.onToastMessageDisplayed()
            }
        }

        composable(INFO_SCREEN) {
            InfoScreen()
        }
    }
}

@Composable
private fun DoubleBackHandler() {
    var lastPressTime by remember { mutableLongStateOf(0L) }
    val context = LocalContext.current

    BackHandler(true) {
        val timeNow = System.currentTimeMillis()
        if (lastPressTime + 2000 < timeNow) {
            lastPressTime = timeNow
            Toast.makeText(context, R.string.toast_back_press, Toast.LENGTH_SHORT)
                .show()
        } else {
            (context as? Activity)?.finish()
        }
    }
}