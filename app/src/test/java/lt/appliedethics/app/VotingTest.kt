package lt.appliedethics.app

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.RoundingMode

class VotingTest {

    @Test
    fun `when odd number of voters then calculate() selects median contribution`() {
        val votes = listOf(
            Vote(contribution = BigDecimal.TEN, isYes = true),
            Vote(contribution = BigDecimal(3), isYes = false),
            Vote(contribution = BigDecimal(0), isYes = false),
        )

        val result = Voting().calculate(votes)

        assertEquals(BigDecimal(3), result.medianContribution)
    }

    @Test
    fun `when even number of voters then calculate() yields median contribution`() {
        val votes = listOf(
            Vote(contribution = BigDecimal.TEN, isYes = true),
            Vote(contribution = BigDecimal(3), isYes = false),
            Vote(contribution = BigDecimal(2), isYes = false),
            Vote(contribution = BigDecimal(0), isYes = false),
        )

        val result = Voting().calculate(votes)

        assertEquals(BigDecimal(2.5), result.medianContribution)
    }

    @Test
    fun `when median contribution would be zero then calculate() selects median contribution`() {
        val votes = listOf(
            Vote(contribution = BigDecimal.TEN, isYes = true),
            Vote(contribution = BigDecimal.ZERO, isYes = false),
            Vote(contribution = BigDecimal.ZERO, isYes = false),
        )

        val result = Voting().calculate(votes)

        assertEquals(BigDecimal(0.1).setScale(1), result.medianContribution)
    }

    @Test
    fun `when calculate() is called then vote coefficients are adjusted`() {
        val votes = listOf(
            Vote(contribution = BigDecimal.TEN, isYes = true),
            Vote(contribution = BigDecimal(3), isYes = false),
            Vote(contribution = BigDecimal(2), isYes = false),
            Vote(contribution = BigDecimal(0), isYes = false),
        )

        val result = Voting().calculate(votes)

        assertEquals(BigDecimal(2.6094).setScale(4, RoundingMode.HALF_UP), result.weightedVotes[0].voteCoefficient)
        assertEquals(BigDecimal(1.7885).setScale(4, RoundingMode.HALF_UP), result.weightedVotes[1].voteCoefficient)
        assertEquals(BigDecimal(1.5878).setScale(4, RoundingMode.HALF_UP), result.weightedVotes[2].voteCoefficient)
        assertEquals(BigDecimal.ONE.setScale(4), result.weightedVotes[3].voteCoefficient)
    }
}